This is the stability analysis of equations 6, 7, and 8 from Kirschner and Panetta (1998).

As should be apparent from the title, this version of the stability analysis is done with respect to a (immune response) instead of c (tumor agenticity).
Vera 3/5/17.