% This program assumes that you ran stability_analysis.m, and saved the
% a_val_array and notable_a_values. These should be loaded in the
% workspace. This program will go through those structs and make vectors
% for plotting.

% This program is modified from bifurcation_digram, which considers c.

y_value_vector = nan(1, 4 * length(a_val_array));
a_value_vector = nan(1, length(y_value_vector));
% One value of c may have up to 4 y equilibria

counter = 1;
for i = 1:length(a_val_array)
    a_value = a_val_array{i}.a_val;
    y_values = a_val_array{i}.y_equilibria;
    for j = 1:length(y_values)
        if isreal(y_values(j))
            a_value_vector(counter) = a_value;
            y_value_vector(counter) = y_values(j);
            counter = counter + 1;
        end
    end
end

plotter_vector = log(y_value_vector);
plot(a_value_vector, plotter_vector, 'p')
ylim([8, 16])
title('Equilibrium Tumor Cells Over Immune Response')
ylabel('Log of Number of Tumor Cells (log(y))')
xlabel('Immune Response (a)')

        
    