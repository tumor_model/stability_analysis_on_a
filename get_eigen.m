function [ edgar ] = get_eigen( equilibrium, tumor_p )
%GET_EIGEN This function takes an equilibrium point and finds the
%associated eigenvalues.

% INPUT:
%   equilibrium: 1 x 3 vector containing an (x, y, z) equilibrium point
%   tumor_p: struct containing the tumor parameters, including any
%       parameters that are varied in the main program.

% OUTPUT:
%   edgar: 3 x 1 column vector containing the eigenvalues found for the
%       equilibrium point

% CALLED BY:
%   stability_analysis
%   stability_analysis_on_a

% CHANGE LOG:
%   Set this up to get all constants from tumor p, allows varying several
%   constants. Vera 3/4/17.

% Values from Table 1
mu_2 = tumor_p.mu_2;
p_1 = tumor_p.p_1;
g_1 = tumor_p.g_1;
g_2 = tumor_p.g_2;
r_2 = tumor_p.r_2;
b = tumor_p.b;
a = tumor_p.a;
mu_3 = tumor_p.mu_3;
p_2 = tumor_p.p_2;
g_3 = tumor_p.g_3;
c = tumor_p.c;
s_1 = tumor_p.s_1;
s_2 = tumor_p.s_2;

% Equilibrium points passed
x_eq = equilibrium(1);
y_eq = equilibrium(2);
z_eq = equilibrium(3);

% Guess what? Matlab has a Jacobian function. Here, I am using the
% equations copied from ODE_system for four_plots. Vera 2/24/17.
syms x y z
jacob = jacobian([c * y - mu_2 * x + p_1 * x .* z ./ (g_1 + z) + s_1, ...
r_2 * y .* (1 - b * y) - a * x .* y ./ (g_2 + y), ...
p_2 * x .* y / (g_3 + y) - mu_3 * z + s_2], [x, y, z]);

% Put the current equilibrium values into the jacobian
jacob = subs(jacob, [x, y, z], [x_eq, y_eq, z_eq]);
% Using jacob to avoid having a variable and a function with the same name

% Convert symbolic values back to doubles
jacob = double(jacob);

% eig returns a column containing the eigenvalues for the provided matrix
% It can return a second variable which is the eigenvectors
try
    edgar = eig(jacob);
catch ME
    % eig does not like a jacobian matrix with NaNs in it. So, we catch
    % those cases and return an empty matrix
    if (strcmp(ME.identifier, 'MATLAB:eig:matrixWithNaNInf'))
        edgar = [];
        %disp('NaNs in the Jacobian')
    elseif (strcmp(ME.identifier, 'symbolic:sym:InputMustNotContainNaNOrInf'))
        edgar = [];
    else
        rethrow ME
    end
end
    
end

