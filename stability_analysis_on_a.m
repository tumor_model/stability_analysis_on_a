% The goal of this is, given a, to find the equilibrium points of the
% system and then calculate the eigenvalues at the points.

% CHANGE LOG
% Added storing the y equilibrium in the c_val_array and notable_c_values.
% This is done to support plotting. Vera 2/28/17.

% Modified this version of the code to allow varying a, while holding c
% fixed. Things like c_val_array, notable_c_values renameed to a_val_array,
% etc. Vera 2/28/17.

% Store the fixed parameters in a struct so we don't have to define them
% multiple times
tumor_p.mu_2 = 0.03;
tumor_p.p_1 = 0.1245;
tumor_p.g_1 = 20000000;
tumor_p.g_2 = 100000;
tumor_p.r_2 = 0.18;
tumor_p.b = 0.000000001;
%tumor_p.a = 1; This is varying now
tumor_p.mu_3 = 10;
tumor_p.p_2 = 5;
tumor_p.g_3 = 1000;

% s_1 and s_2 represent treatments, at this stage we are not looking at
% any treatments so these are both zeros
tumor_p.s_1 = 0;
tumor_p.s_2 = 0;

tumor_p.c = 0.035; % Chose an interesting value of c
% This is the value shown in four plots when the amplitude decays with
% time.

a_values = 0:0.05:2; % The range is not given in the paper, so I'm guessing at a
% good range here

% Preallocate arrays to save all our cool results
a_val_array = {1, length(a_values)};

% The idea is to save information on the equilibria and their eigenvalues
% associated with each value of c. While in the loop, we try to capture
% some information about the number and types of equilibria.
notable_a_values = {};
notable_counter = 1;

% Initialize a dummy previous struct so MATLAB won't get pissy
previous_a_struct.c_val = NaN;
previous_a_struct.total_equilibria = NaN;
previous_a_struct.num_relevant_equilibria = NaN;
previous_a_struct.eigen_array = NaN;
previous_a_struct.imaginary_eigenvalues = NaN;
previous_a_struct.negative_eigenvalues = NaN;
previous_a_struct.zero_eigenvalues = NaN;

for i = 1:length(a_values)
    
    notable_struct.a_value = -1; % If this value is unchanged, we know nothing
    % changed from the last a value
    
    a = a_values(i);
    
    % Update tumor_p for passing into functions. Vera 3/4/17.
    tumor_p.a = a;
    
    % Inialize the a_val_struct
    a_val_struct.a_val = a;
    
    % Get the struct with all equilibria
    eq_struct = get_equilibrium(tumor_p);
    
    % Save total number of equilibria
    a_val_struct.total_equilibria = length(eq_struct.x);
    
    eq_matrix = prune_equilibria(eq_struct);
    
    % Compare the number of equilibria to the previous c value
    if previous_a_struct.total_equilibria ~= a_val_struct.total_equilibria
        notable_struct.a_value = a;
        notable_struct.new_total_equilibria = a_val_struct.total_equilibria;
        notable_struct.old_total_equilibria = previous_a_struct.total_equilibria;
        
        % Also store the y values
        notable_struct.y_equilibria = eq_matrix(:, 2);
    end
    [num_equilibria, ~] = size(eq_matrix);
    a_val_struct.num_relevant_equilibria = num_equilibria;
    
    a_val_struct.y_equilibria = eq_matrix(:, 2);
    % Grab the second column of the eq_matrix, this is the y value for the
    % equilibrium (y is the amount of cancer). We want this for plotting.
    
    % Compare the number of equilibria in the relevant range (equal to or
    % greater than zero)
    if previous_a_struct.num_relevant_equilibria ~= a_val_struct.num_relevant_equilibria
        notable_struct.a_value = a;
        notable_struct.new_relevant_equilibria = a_val_struct.num_relevant_equilibria;
        notable_struct.old_relevant_equilibria = previous_a_struct.num_relevant_equilibria;
        
        % Also store the y values
        notable_struct.y_equilibria = eq_matrix(:, 2);
    end
    
    % Get the eigenvalues associated with each equilibrium found
    eigen_array = {};
    imaginary_eigenvalues = {};
    negative_eigenvalues = {};
    zero_eigenvalues = {};
    
    for j = 1:num_equilibria
        equilibrium = eq_matrix(j, :);
        eigen_column = get_eigen(equilibrium, tumor_p);
        eigen_array{j} = eigen_column;
        
        % Count the number of imaginary and negative eigenvalues
        num_imaginary = 0;
        num_negative = 0;
        num_zero = 0;
        for k = 1:length(eigen_column)
            
            % Count the eigenvalues that are zero or negative
            if eigen_column(k) == 0
                num_zero = num_zero + 1;
            elseif eigen_column(k) < 0
                num_negative = num_negative + 1;
            end
            
            % Count the eigenvalues that are imaginary
            if ~isreal(eigen_column(k))
                num_imaginary = num_imaginary + 1;
            end
        end
        imaginary_eigenvalues{j} = num_imaginary;
        negative_eigenvalues{j} = num_negative;
        zero_eigenvalues{j} = num_zero;
    end
    
    a_val_struct.eigen_array = eigen_array;
    a_val_struct.imaginary_eigenvalues = imaginary_eigenvalues;
    a_val_struct.negative_eigenvalues = negative_eigenvalues;
    a_val_struct.zero_eigenvalues = zero_eigenvalues;
    
    % Check for differences in the eigenvalues of the current and previous
    % c values
    if ~isequaln(previous_a_struct.imaginary_eigenvalues, a_val_struct.imaginary_eigenvalues)
        notable_struct.a_value = a;
        notable_struct.new_imaginary_eigenvalues = a_val_struct.imaginary_eigenvalues;
        notable_struct.old_imaginary_eigenvalues = previous_a_struct.imaginary_eigenvalues;
        
        % Also store the y values
        notable_struct.y_equilibria = eq_matrix(:, 2);
    end
    
    if ~isequaln(previous_a_struct.negative_eigenvalues, a_val_struct.negative_eigenvalues)
        notable_struct.a_value = a;
        notable_struct.new_negative_eigenvalues = a_val_struct.negative_eigenvalues;
        notable_struct.old_negative_eigenvalues = previous_a_struct.negative_eigenvalues;
        
        % Also store the y values
        notable_struct.y_equilibria = eq_matrix(:, 2);
    end
    
    if ~isequaln(previous_a_struct.zero_eigenvalues, a_val_struct.zero_eigenvalues)
        notable_struct.a_value = a;
        notable_struct.new_zero_eigenvalues = a_val_struct.zero_eigenvalues;
        notable_struct.old_zero_eigenvalues = previous_a_struct.zero_eigenvalues;
        
        % Also store the y values
        notable_struct.y_equilibria = eq_matrix(:, 2);
    end
    
    % Save the struct in the array
    a_val_array{i} = a_val_struct;
    
    % Save the struct to compare on the next round. Note that this is
    % replaced on each loop
    previous_a_struct = a_val_struct;
    
    % Check if anything got stored as "notable" and save the notable struct
    % if so
    if notable_struct.a_value ~= -1
        notable_a_values{notable_counter} = notable_struct;
        notable_counter = notable_counter + 1;
    end
    
    % Clear the notable struct out of the workspace so it won't propagate
    % extra fields
    clear notable_struct
    
end


